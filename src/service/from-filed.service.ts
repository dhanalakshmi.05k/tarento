import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FromFiledService {

  constructor() { }
  //sample valid json payload to render the form
  public  flexiConfig = {

    "items": [
      {
        "name": "personname",
        "label": "Person's Name",
        "type": "TextField"
      },
      {
        "name": "states",
        "label": "Person's state",
        "type": "DropDown",
        "values": [
          "Maharashtra",
          "Kerala",
          "Tamil Nadu"
        ]
      }
    ]
  }
}
