import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromGeneratorComponent } from './from-generator.component';

describe('FromGeneratorComponent', () => {
  let component: FromGeneratorComponent;
  let fixture: ComponentFixture<FromGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
