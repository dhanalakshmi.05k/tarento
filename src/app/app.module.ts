import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ConfigFromGeneratorComponent } from './config-from-generator/config-from-generator.component';
import { FromGeneratorComponent } from './commonComponent/from-generator/from-generator.component';
import {FromFiledService} from "../service/from-filed.service";
import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    ConfigFromGeneratorComponent,
    FromGeneratorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [FromFiledService],
  bootstrap: [AppComponent]
})
export class AppModule { }
