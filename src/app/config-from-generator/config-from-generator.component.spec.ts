import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigFromGeneratorComponent } from './config-from-generator.component';

describe('ConfigFromGeneratorComponent', () => {
  let component: ConfigFromGeneratorComponent;
  let fixture: ComponentFixture<ConfigFromGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigFromGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigFromGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
