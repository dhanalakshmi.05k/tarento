import { Component, OnInit } from '@angular/core';
import {FromFiledService} from "../../service/from-filed.service";

@Component({
  selector: 'app-config-from-generator',
  templateUrl: './config-from-generator.component.html',
  styleUrls: ['./config-from-generator.component.css']
})
export class ConfigFromGeneratorComponent implements OnInit {
  public fromConfig:any={}
  constructor(private fromFiledService:FromFiledService) { }

  ngOnInit() {
    this.getFormParameterConfigDetails();
  }

  getFormParameterConfigDetails() {

    this.fromConfig=this.fromFiledService.flexiConfig;
  }


}
