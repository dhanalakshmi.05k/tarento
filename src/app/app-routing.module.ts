import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConfigFromGeneratorComponent} from "./config-from-generator/config-from-generator.component";
import {RouterModule, Routes} from "@angular/router";
const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {path: 'home', component: ConfigFromGeneratorComponent}
];






@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
